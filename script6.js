let btn = document.querySelector('#btn-submit');
let choices = document.querySelector('#choices');
let scoreContainer = document.querySelector('#score');
let questionContainer = document.querySelector('#question');
let score = [];
const questionBank = [
  {
    question: 'Question 1',
    choices: {
      1: 'option 1',
      2: 'option 2',
      3: 'option 3',
      4: 'option 4'
    },
    answer: 3
  },
  {
    question: 'Question 2',
    choices: {
      1: 'option 1',
      2: 'option 2',
      3: 'option 3',
      4: 'option 4'
    },
    answer: 2
  },
  {
    question: 'Question 3',
    choices: {
      1: 'option 1',
      2: 'option 2',
      3: 'option 3',
      4: 'option 4'
    },
    answer: 1
  }
];

btn.addEventListener('click', function() {
  //call findItem function and pass in the length of the score array which is in getScore function
  //__________________________
  //find the input element with the attribute name "option" that is checked
  let answer = document.querySelector('input[name="__________"]:checked');

  //if it exists, call checkAnswer function and pass in two arguments: the length of the score array and the answer element's VALUE (Note: answer element is an input element)
  if (answer) {
    checkAnswer(getScore(), _________________);
  }
});

function getScore() {
  //return the length of the score array
  ______________________________;
}

function findItem(indexPosition) {
  try {
    //find the item in the questionBank array through its index position
    let item = _______________________;
    //if it exists, call createItem function and pass in the item that you have found
    createItem(item);
  } catch (e) {
    //if e instance of TypeError
    if (e instanceof TypeError) {
      // use reducer method to add the values inside score array and get your final score; see documentation
      const reducer = ______________________________________________;
      let finalScore = ____________________________________;
      // display final score and message inside questionContainer element; see line 4
      //____________________________________________
    }
  }
}

function createNewElement(element, text) {
  //create element and pass in the element parameter
  //_______________________________________
  //add text content to the created element and pass in text parameter
  //______________________________________
  //return created element
  //_______________________________________
}

function createChoice(value) {
  //call createElement function to create input element
  let input = ___________________________;
  /*
    set attributes for input so that it will produce the following result
    <input class="form-check-input position-static mr-3" type="checkbox" name="option" value="value">

    Note that you need to pass in the value parameter when setting attribute for value
  */
  //______________________________________
  //______________________________________
  //______________________________________
  //______________________________________
  //return input
  //______________________________________
}

function createItem(item) {
  //assign item.question to questionContainer's inner text
  questionContainer.innerText = _____________________________;
  let formCheck;
  let label;
  let span;
  //create a loop. 4 because you have 4 choices
  for (let x = 1; x <= 4; x++) {
    /* 
      call createNewElement function to create div, label, and span
      call createChoice function an pass in x as value
      set appropriate attributes and do appropraite appendChild method to create the following result:

      <div class="form-check">
          <label>
            <input class="form-check-input position-static mr-3" type="checkbox" name="option" value="1">
            <span>option 1</span>
          </label>
      </div>
    */
    formCheck = createNewElement('__________');
    formCheck.setAttribute('class', '_______________');
    label = createNewElement('_______________');
    input = createChoice(x);
    span = createNewElement('span', item.choices[_____________]);
    //line 128 to 131 simply requires you to append
    //______________________________________
    //______________________________________
    //______________________________________
    //______________________________________
  }

  //return choices
}

function checkAnswer(indexPosition, answer) {
  //create correctAnswer variable and assign it to the answer property in questionBank array
  let correctAnswer = questionBank[___________________].answer;
  //if the correctAnswer is equal to the answer
  if (_______________ == _________________) {
    //push 1 to score array
    //______________
  } else {
    //push 0 to score array
    //______________
  }
  //remove content inside choices by assigning it's innerHTML to a blank string
  //__________________________
  //call findItem function and pass in the length of the score array
  //_______________________________
}
